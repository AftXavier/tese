from MAC import mac_base
from matplotlib import pyplot as plt

fig,ax = plt.subplots(2,figsize=(15,7.5))
fig.suptitle("Duty Cycle and Power regulations according to ERC70-03",fontsize=16)
fig.subplots_adjust(hspace=.5)
DCs = []
ptxs = []
ax[0].set_title('Duty Cycle')

ax[0].set_xlim((863000,870000))
ax[1].set_xlim((863000,870000))
#ax[0].set_ylim((0,100))
ax[0].set_yscale('log')
ax[0].set_xlabel('Frequency')
ax[0].set_xticks(range(863000,870000,1000))
ax[1].set_title('Ptx in dBm')
ax[1].set_xlabel('Frequency')
ax[1].set_ylabel('dBm')
ax[1].set_ylim((0,28))
for f in range(863000,870000,1):
    regs = mac_base.Mac.regulatory_parameters_from_frequency(f)
    DC,ptx,sub = regs
    DCs.append(DC*100)
    ptxs.append(ptx)
ax[0].plot(range(863000,870000,1),DCs, label = 'Duty Cycled operation')
ax[1].plot(range(863000,870000,1),ptxs)
ax[0].plot([865000,868000],[1,1],color = 'r', label = 'Alternatives')

ax[1].plot([869700,870000],[14,14],color = 'r')
ax[0].plot([869700,870000],[1,1],color = 'r')
ax[0].plot([865000,865000],[0,1],color= 'r')
ax[0].text(862000, 0.1, '3.6 secs/hour',fontsize=10)
ax[0].text(862000, 1, '36 secs/hour',fontsize=10 )
ax[0].text(862000, 10, ' 360 secs/hour',fontsize=10 )
ax[0].text(862000, 100, '3600 secs/hour',fontsize=10 )

ax[0].text(864000, 0.15, 'h1.3',fontsize=10)
ax[0].text(868200, 1.5, 'h1.4',fontsize=10 )
ax[0].text(868900, 0.15, 'h1.5',fontsize=10 )
ax[0].text(869400, 15, 'h1.6',fontsize=10 )
ax[0].text(869800, 110, 'h1.7',fontsize=10 )
ax[0].text(866600, 1.5, 'h1.3',fontsize=10 )
ax[0].text(869750, 1.5, 'h1.7',fontsize=10 )
ax[0].plot([863000,870000],[2.8,2.8],label = 'LBT + AFA operation')
for xc in range(863000,870000,200):
    ax[0].axvline(x=xc,linestyle='--', color ='g',lw=0.3)
    ax[0].legend(loc=(0,-0.4))
    ax[1].axvline(x=xc,linestyle='--', color ='g',lw=0.3)

plt.show()
fig.savefig('plot_regulations.png')
