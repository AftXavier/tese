from struct import pack
from math import ceil, log10, sqrt
from simpy import Resource, Interrupt
from collections import deque
import logging

gamma = 0
LPLd0 = 0
d0 = 0

def uniformly_distributed_frequencies_with_margins(cf, radio_bw, n_rx_channels):
    if n_rx_channels > 0:
        channel_bw = radio_bw/n_rx_channels
        aux_frequency = cf - (radio_bw / 2) + (channel_bw / 2)
        channels = []
        for i in range(n_rx_channels):
            channels.append(aux_frequency)
            aux_frequency = aux_frequency + channel_bw
    else:
        channels = []
        channels.append(cf)
    return channels


def max_distances(station, ptx=14):
    result = (d0*(10**((ptx-station.sf12[0]-LPLd0 - station.noise_figure)/(10.0*gamma))),
              d0*(10**((ptx-station.sf11[0]-LPLd0 - station.noise_figure)/(10.0*gamma))),
              d0*(10**((ptx-station.sf10[0]-LPLd0 - station.noise_figure)/(10.0*gamma))),
              d0*(10**((ptx-station.sf9[0]-LPLd0 - station.noise_figure)/(10.0*gamma))),
              d0*(10**((ptx-station.sf8[0]-LPLd0 - station.noise_figure)/(10.0*gamma))),
              d0*(10**((ptx-station.sf7[0]-LPLd0 - station.noise_figure)/(10.0*gamma))))
    return result


def distance(t1, t2):
    x1 = t1.x
    x2 = t2.x
    y1 = t1.y
    y2 = t2.y
    d = sqrt(((x1-x2)**2)+(y1-y2)**2)
    if d == 0:
        t1.y += 1
        t1.x += 1
        return distance(t1, t2)
    else:
        return d


def lpl(d):
    return LPLd0 + 10*gamma * log10(d/d0)


def dbm_to_w(ptx_dbm):
    return 0.001 * 10**(ptx_dbm/10)


def sensitivity_bw(bw):
    return{
        125: 0,
        250: 1,
        500: 2,
    }[bw]

class Packet(object):

    @classmethod
    def payload_size_given_airtime_and_params(cls, params, max_airtime):
        # params(cf,sf,bw,cr)
        p = Packet(params, payload='')
        for i in range(1, 255):
            p.payload = pack('c' * i, *[b'a' for x in range(i)])  # add a byte each iteration
            if p.airtime > max_airtime:
                return i - 1
        return None

    def __init__(self, param, payload=None, with_header=True, ptx=0):
        self.param = param  # (CF, SF, BW, CR)
        self.payload = ''.encode()
        self.header = with_header
        if payload is not None:
            self.payload = payload
        self.prx = 0
        self.ptx = ptx
        if self.param[1] == 11 or self.param[1] == 12:
            self.DE = 1
        else:
            self.DE = 0

    @property
    def airtime(self):
        cf, sf, bw, cr = self.param
        pl = len(self.payload)
        h = 1
        if self.header:
            h = 0   # H=0 means header is sent first, (H=1) means header is in the payload, receiver must know length
        de = 0  # low data rate optimization enabled (=1) or not (=0)
        npream = 8
        tsym = (2.0 ** sf) / bw
        tpream = (npream + 4.25) * tsym
        payloadsymbnb = 8 + max(ceil((8.0 * pl - 4.0 * sf + 28 + 16 - 20 * h) / (4.0 * (sf - 2 * self.DE))) * (cr + 4), 0)
        tpayload = payloadsymbnb * tsym
        return tpream + tpayload

    def __str__(self):
        return "channel: " + str(self.param) +" payload: " + str(self.payload) + " airtime: " + str(self.airtime)

    @property
    def energy_for_tx_heuristic(self):
        # cheating
        tx_current = lambda pdbm: 1.173737973 * pdbm + 21.83836943 # milli Amperes

        energy = tx_current(self.ptx) * self.airtime  # heuristic

        return energy

    def copy(self):
        p = Packet(self.param, self.payload, self.header,self.ptx)
        return p


class Demodulator(object):
    def __init__(self, env, packet, radio, dem_list):
        self.param = packet.param
        self.radio = radio
        self.collided = False

        env.process(self.finish(env, packet.airtime, packet, radio, dem_list))


    def finish(self, env, time, packet, radio, dem_list):
        yield env.timeout(time)
        if not self.collided and len(self.radio.demodulators) > 0:  # if no collision and we are still receiving

            self.radio.node.received += 1
            radio.received_packets.append((env.now, packet))
            radio.received_flag.succeed()
            radio.received_flag = env.event()
            dem_list.remove(self)
            logger = logging.getLogger('sim')
            logger.warning("[PHY] %f | %d received %s %s with prx %f" % (
                env.now / 1000, self.radio.node.id, packet.payload, str(packet.param), packet.prx))
        else:
            dem_list.remove(self)
            self.radio.node.collisions += 1

    def check_collision(self, packet):
        if self.frequencycollision(packet) and self.sfcollision(packet):
            self.collided = True
            return 1
        return 0

    def frequencycollision(self, other):
        if abs(self.param[0] - other.param[0]) <= 240 and (self.param[2] == 500 or other.param[2] == 500):
            return True
        if abs(self.param[0] - other.param[0]) <= 120 and (self.param[2] == 250 or other.param[2] == 250):
            return True
        if abs(self.param[0] - other.param[0]) <= 60 and (self.param[2] == 125 or other.param[2] == 125):
            return True
        return False

    def sfcollision(self, other):
        if self.param[1] == other.param[1]:
            return True
        return False


def transmission(origin, env, pkt):
    ptx = pkt.ptx
    for (r, lpl) in [n for n in origin.neighbours if n[0].receiving]:
        packet = pkt.copy()
        packet.prx = ptx - lpl - r.node.noise_figure

        env.process(r.packets_on_air(packet))  ## t in transmission range gets the packet so as to make LBT implementation possible
        if packet.prx >= r.s[int(packet.param[1] - 7)][sensitivity_bw(packet.param[2])]:
            r.receive(packet)


class Lora_radio(object):
    radios = []

    def __init__(self, env, x, y, n_multi_sf_demodulators, n_single_demodulators, s, node,bw):
        self.x, self.y, self.env = x, y, env
        self.s = s
        Lora_radio.radios.append(self)
        self.neighbours = None
        self.node = node
        self.total_bandwidth = bw

        self.antenna = Resource(env, capacity=1)  # can only send one at a time
        self.antenna_request = None

        self.multi_sf_demodulators = []
        self.multi_sf_rx_channels = []  # list of tuples with cf, sf, bw, cr
        self.max_multi_sf_demodulators = n_multi_sf_demodulators

        self.single_demodulators = []
        self.max_single_demodulators = n_single_demodulators
        self.single_sf_rx_channels = []

        self.received_packets = deque()

        self.received_flag = False
        self.receiving = 0
        self.rec_start = None
        self.rec_finish = None

        self.p_on_air = []  # list of tuples with cf,sf  #### cheating

    def add_multi_sf_rx_channel(self, cf):
        for sf in range(7, 13):
            self.multi_sf_rx_channels.append((cf, sf, 125, 1))

    def set_channel(self, param):
        self.single_sf_rx_channels = []
        self.multi_sf_rx_channels = []
        next_cf = param[0]
        for i in range(self.max_single_demodulators):
            self.single_sf_rx_channels.append((next_cf, param[1], param[2], param[3]))  # (cf,sf,bw,cr)
            next_cf = ceil(next_cf - int(param[2]*1.5))

        if self.max_multi_sf_demodulators != 0:
            cfs = uniformly_distributed_frequencies_with_margins(param[0], self.total_bandwidth,
                                                                 self.max_multi_sf_demodulators)
            for c in cfs:
                self.add_multi_sf_rx_channel(c)
    @property
    def demodulators(self):
        return self.multi_sf_demodulators + self.single_demodulators

    @property
    def rx_channels(self):
        return self.multi_sf_rx_channels + self.single_sf_rx_channels

    @property
    def n_channels(self):
        if self.max_multi_sf_demodulators == 0:
            return 1
        return self.max_multi_sf_demodulators

    def start_receiving(self, env):
        self.antenna_request = self.antenna.request()
        yield self.antenna_request
        self.receiving = 1
        self.rec_start = env.now
        self.received_flag = env.event()
        #logger.debug("[PHY] %d | %d started receiving" % (ceil(env.now / 1000), self.node.id))

    def stop_receiving(self, env):
        logger = logging.getLogger('sim')
        self.receiving = 0
        self.received_flag = None
        self.antenna.release(self.antenna_request)
        self.rec_finish = env.now
        self.node.consume_energy_rx((self.rec_finish - self.rec_start) / 1000)
        if len (self.single_demodulators ) > 0:
            logger.warning("[PHY] %f | %d lost %d packets when changing channel" % (env.now / 1000, self.node.id, len(self.single_demodulators)))
            self.node.lost_changing_channel += 1

        if len (self.multi_sf_demodulators) > 0:
            logger.warning("[PHY] %f | %d lost %d packets when changing channel" % (env.now / 1000, self.node.id, len(self.multi_sf_demodulators)))

        self.single_demodulators = []
        self.multi_sf_demodulators = []
        #logger.debug("[PHY] %d | %d stopped receiving" % (ceil(env.now / 1000), self.node.id))

    def receive(self, packet):  ## method called by transmission process created by a sening node
        collisions = 0
        for d in self.demodulators:
            collisions += d.check_collision(packet)
        if collisions == 0:
            if packet.param in self.single_sf_rx_channels:
                if len(self.single_demodulators) < self.max_single_demodulators:  ## normal channel with fixed cf,sf,bw,cr
                    self.single_demodulators.append(Demodulator(self.env, packet, self, self.single_demodulators))
            elif packet.param in self.multi_sf_rx_channels :
                if len(self.multi_sf_demodulators) < self.max_multi_sf_demodulators:
                    self.multi_sf_demodulators.append(Demodulator(self.env, packet, self, self.multi_sf_demodulators))
        else:
            logger = logging.getLogger('sim')

            logger.warning("%d | %d collided: %s, params: %s" %
                        (ceil(self.env.now / 1000), self.node.id, packet.payload, str(packet.param)))
            self.node.collisions += 1

    def send(self, payload, params=None, ptx=14):
            with self.antenna.request() as req:
                yield req
                packet = Packet(params, payload, ptx=14)
                transmission(self, self.env, packet)
                yield self.env.timeout(packet.airtime)
                self.antenna.release(req)
                self.node.consume_energy_tx(ptx, packet.airtime / 1000)
                self.node.sent += 1
                logger = logging.getLogger('sim')
                logger.warning(
                "[PHY] %f | %d sent %s with params %s and ptx %d, airtime %f" % (self.env.now / 1000, self.node.id, payload,
                                                                     str(params),ptx, packet.airtime))
            return packet

    def get_neighbours(self):  ## transmission only pokes nodes in this list
        self.neighbours = tuple((r, lpl(distance(self, r))) for r in Lora_radio.radios if (r != self and
                                                                                           14 - lpl(distance(self, r)) >
                                                                                           r.s[5][0]))

    def get_received_packet(self):
        try:
            try:
                return self.received_packets.popleft()
            except IndexError:
                yield self.received_flag
                return self.received_packets.popleft()
        except Interrupt:
            pass

    def packets_on_air(self, packet):
        a = packet.param[:2]  # cf,sf for collision checking
        self.p_on_air.append(a)
        yield self.env.timeout(packet.airtime)
        self.p_on_air.remove(a)

    def receive_during_seconds(self, seconds):
        yield self.env.process(self.start_receiving(self.env))
        yield self.env.timeout(seconds * 1000)
        self.stop_receiving(self.env)

    def clear_channel_assessment(self, c, duration):
        env = self.env
        start = env.now
        cf = c[0]
        sf = c[1]
        self.set_channel(c)
        yield env.process(self.start_receiving(env))
        while env.now - start < duration:
            yield env.timeout(ceil(duration / 5))
            if (cf, sf) in self.p_on_air:  # if there are packets in this cf with this sf
                self.stop_receiving(env)
                return False
            self.stop_receiving(env)
        return True
