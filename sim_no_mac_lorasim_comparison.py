import settings
import simpy
import logging
import sys
import os
import radio
from math import ceil
from matplotlib import pyplot as plt
from random import randrange, expovariate
from math import ceil, floor
from struct import pack, unpack
from collections import deque



env = settings.env

channel = (randrange(865000, 868600), 12, 125, 4)


def string_to_ms(rt):
    runtime = 0
    if rt.isnumeric():
        runtime = int(rt)
    elif 'd'in rt:
        runtime = int(rt[:-1])*60*60*24*1000
    elif 'h' in rt:
        runtime = int(rt[:-1])*60*60*1000
    elif 'm' in rt:
        runtime = int(rt[:-1])*60*1000
    elif 's' in rt:
        runtime = int(rt[:-1])*1000
    return runtime


class Sx1301BS:
    sf7 = [-126, -123, -120]
    sf8 = [-129, -126, -123]
    sf9 = [-131, -128, -125]
    sf10 = [-134, -131, -128]
    sf11 = [-136, -133, -130]
    sf12 = [-139, -136, -133]
    s = [sf7, sf8, sf9, sf10, sf11, sf12]
    noise_figure = 3  # db   For noise in the area of deployment of the node

    def __init__(self, id, x, y, n_single_demodulators, n_multi_demodulators, sf_mask):
        self.x = x
        self.y = y
        self.id = id
        self.radio = radio.Lora_radio(env, x, y,n_multi_demodulators, n_single_demodulators,
                                      self.s, self, 1600)

        # Event counters
        self.sent = 0
        self.received = 0
        self.collisions = 0
        self.lost_changing_channel = 0

    def run(self):
        yield env.timeout(1)
        self.radio.get_neighbours()  # find radios in transmission range
        self.radio.set_channel(channel)
        yield env.process(self.radio.start_receiving(env))
        while True:
            yield env.process(self.radio.get_received_packet())

    def consume_energy_tx(self, power, airtime):
        pass

    def consume_energy_rx(self, energy):
        pass


class Sx127x:  # SX1272

    sf7 = [-123, -120, -116]
    sf8 = [-126, -123, -119]
    sf9 = [-129, -125, -122]
    sf10 = [-132, -128, -125]
    sf11 = [-133, -130, -128]
    sf12 = [-136, -133, -130]
    s = [sf7, sf8, sf9, sf10, sf11, sf12]
    VDD = 3  # V
    Capacity = 2  # Ah
    rx_current = 0.0142  # Amps   from the specification of RN2483
    node_max_period = 0
    noise_figure = 3  # db   For noise in the area of deployment of the node
    # MCU plus sensor current
    sensing_routine_consumption = 0.04651  # Joules
    sensing_routine_duration = 5.251  # seconds

    def __init__(self, id, x, y, n_single_demodulators):
        self.x = x
        self.y = y
        self.id = id

        self.radio = radio.Lora_radio(env, x, y, 0, n_single_demodulators, self.s, self, 125)
        self.battery = self.VDD * self.Capacity * 3600  # W x s= J
        self.battery_full = self.battery
        self.energy_from_rx = 0
        self.energy_from_tx = 0

        # Event counters
        self.sent = 0
        self.collisions = 0
        self.received = 0

        # linear regression from the values in RN2483 datasheet
        # Correlation Coefficient: r = 0.9992239468
        # Residual Sum of Squares: rss = 1.234618955
        # Coefficient of Determination: R2 = 0.9984484958

        self.tx_current = lambda pdbm: 1.173737973 * pdbm + 21.83836943  # milli Amperes

    def run(self):
        yield env.timeout(1)
        self.radio.get_neighbours()
        #yield env.timeout(randrange(self.node_max_period))
        while True:

            yield env.timeout(floor(expovariate(1.0 / float(self.node_max_period))))
            msg = pack('B'*20, 1, 2, 3, 4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20)
            yield env.process(self.radio.send(msg, params=channel, ptx=14))
            #yield env.timeout(self.node_max_period)

    def consume_energy_tx(self, power, airtime):
        energy = self.VDD * self.tx_current(power)/1000 * airtime
        self.battery = self.battery - energy
        self.energy_from_tx += energy

    def consume_energy_rx(self, airtime):
        self.battery = self.battery - (self.VDD * self.rx_current * airtime)
        self.energy_from_rx += self.VDD * self.rx_current * airtime

    def __str__(self):
        return '    NODE ' + str(self.id)


def plot(size_x, size_y, nodes, bs):
    fig, ax = plt.subplots()

    ax.set_title("Spatial distribution of Nodes and Base Stations ")
    ax.set_xlim((0, size_x))
    ax.set_xlabel("position in meters")
    ax.set_ylim((0, size_y))
    ax.set_ylabel("position in meters")
    ax.set_xticks(range(0, size_x, ceil(size_x / 5)))
    ax.set_yticks(range(0, size_y, ceil(size_y / 5)))
    m_d = radio.max_distances(Sx127x)
    for b in bs:
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[0],color='r',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[1],color='m',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[2],color='b',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[3],color='g',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[4],color='c',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[5],color='y',fill=False))
        ax.text(b.radio.x,b.radio.y,str(b.id),fontsize=5)
    for n in nodes:
        ax.text(n.radio.x, n.radio.y, str(n.id), fontsize=5)
    plt.grid()
    #plt.show()
    fig.savefig('plotcircles.png')


def ms_to_human(milisecs):
    secs=milisecs/1000
    minutes=secs/60
    hours = minutes/60
    days = hours/24
    current_day_hours=hours%24
    current_hour_minutes=minutes%60
    return str( floor(days))+'d '+str(floor(current_day_hours))+'h '+str(floor(current_hour_minutes))+'m'


def print_time(init):
    while True:
        yield env.timeout(60 * 10 * 1000)
        print(ms_to_human(env.now - init))

if __name__ == "__main__":
    nodes = []
    bs = []

    if len(sys.argv) < 5:
        print("usage: python sim.py station_positions.txt runtime LogLevel NodePeriod")
        exit()

    runtime = string_to_ms(sys.argv[2])
    Sx127x.node_max_period = string_to_ms(sys.argv[4])
    f = open(sys.argv[1])
    content = f.readlines()
    i = 0
    for l_ in content:
        l = l_.split()
        if 'size_x' in l:
            size_x = int(l[1])
        elif 'size_y' in l:
            size_y = int(l[1])

        elif 'SX1272' in l:
            if l[3] == 'Node':
                n = Sx127x(i, int(l[1]), int(l[2]), 0)
                nodes.append(n)
            elif l[6] == 'Bs':
                n = Sx127x(i, int(l[1]), int(l[2]), int(l[5], 16))
                bs.append(n)
            env.process(n.run())
            i += 1
        elif 'SX1301' in l:
            b = Sx1301BS(i, int(l[1]), int(l[2]), int(l[3]), int(l[4]), int(l[5], 16))
            bs.append(b)
            env.process(b.run())
        elif 'gamma' in l:
            radio.gamma = float(l[1])
        elif 'd0' in l:
            radio.d0 = int(l[1])
        elif 'LPLD0' in l:
            radio.LPLd0 = float(l[1])
        elif '#' in l:
            pass

    logger = logging.getLogger('sim')
    logger.setLevel(logging.sys.argv[3])
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.sys.argv[3])
    logger.addHandler(ch)

    env.run(until=runtime)
    bat_full = nodes[0].battery_full
    min_battery = bat_full
    battery_accumulated = 0
    connected = 0
    for n in nodes:
        battery_accumulated += n.battery
        if n.battery < min_battery:
            min_battery = n.battery

    results_nodes = [(n.sent, n.collisions, n.energy_from_rx, n.energy_from_tx) for n in nodes]
    print(len(nodes))
    results_bs = [(b.sent, b.received, b.collisions) for b in bs]

    total_results_nodes = tuple(map(sum, zip(*results_nodes)))
    total_energy = total_results_nodes[-1] + total_results_nodes[-2]
    ratio_rx_total = total_results_nodes[-2]/total_energy
    total_results_bs = tuple(map(sum, zip(*results_bs)))
    print("Report:")
    print("average battery percentage: ", round(100*(battery_accumulated/len(nodes))/bat_full, 3))
    print("\tSent: %d \n\tCollisions: %d \
        \n\t Energy from receptions: %f\n\tEnergy from transmission: %f\
          \n\t Energy reception percentage: %f" % (*total_results_nodes, ratio_rx_total * 100))
    print("BS:")
    print("\tSent: %d \n\tReceived: %d \n\tCollisions: %d " %
          total_results_bs)

    print("\nTotal: \n\tLost uplinks: %d" %(total_results_nodes[0] - total_results_bs[1]))
    print("Packet Extraction:", total_results_bs[1] * 100 / total_results_nodes[0])
    print("Minimum battery percentage: ", round(min_battery*100/bat_full, 3))
    print("Expected Network LifeTime: ", ms_to_human( runtime * bat_full / (bat_full - battery_accumulated/len(nodes))))

    plot(size_x, size_y, nodes, bs)





