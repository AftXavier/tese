import settings
import logging
import sys
import os
import radio
from matplotlib import pyplot as plt
from random import randrange, expovariate
from math import ceil, floor
import MAC

env = settings.env


class Network_server:

    def __init__(self, network_id, delay):
        self.id = network_id
        self.NETWORK_KEY = os.urandom(16)
        self.auth_code = os.urandom(4)
        self.MAC_DEFAULT_NW_LINK = (randrange(869475, 869575), 12, 125, 1)  # 10 % duty cycle on this subband
        self.bs = []
        self.nodes = []

        self.delay = delay


def string_to_ms(rt):
    runtime = 0
    if rt.isnumeric():
        runtime = int(rt)
    elif 'd'in rt:
        runtime = int(rt[:-1])*60*60*24*1000
    elif 'h' in rt:
        runtime = int(rt[:-1])*60*60*1000
    elif 'm' in rt:
        runtime = int(rt[:-1])*60*1000
    elif 's' in rt:
        runtime = int(rt[:-1])*1000
    return runtime

class Sx1301BS:
    sf7 = [-126, -123, -120]
    sf8 = [-129, -126, -123]
    sf9 = [-131, -128, -125]
    sf10 = [-134, -131, -128]
    sf11 = [-136, -133, -130]
    sf12 = [-139, -136, -133]
    s = [sf7, sf8, sf9, sf10, sf11, sf12]
    noise_figure = 3  # db   for noise in the area of deployment of the BS

    def __init__(self, id, x, y, n_single_demodulators, n_multi_demodulators, sf_mask, network, role):
        self.x = x
        self.y = y
        self.id = id
        self.radio = radio.Lora_radio(env, x, y, n_multi_demodulators
                                      , n_single_demodulators, self.s, self, 1600)

        # Event counters
        self.sent = 0
        self.received = 0
        self.collisions = 0
        self.lost_changing_channel = 0
        self.mac = role(self, sf_mask, network)
        self.battery = 0
        self.battery_full = 0

        # Mac variables

    def run(self):
        yield env.timeout(1)
        self.radio.get_neighbours()  # find radios in transmission range
        env.process(self.mac.run())

    def consume_energy_tx(self, power, airtime):
        pass

    def consume_energy_rx(self, duration):
        pass

class SX127x:
    # Sensitivities from the SX1276 modem employed in the RN2483, receiver losses of -6 dB are already taken into account
    sf7 = [-123, -120, -116]
    sf8 = [-126, -123, -119]
    sf9 = [-129, -125, -122]
    sf10 = [-132, -128, -125]
    sf11 = [-133, -130, -128]
    sf12 = [-136, -133, -130]
    s = [sf7, sf8, sf9, sf10, sf11, sf12]
    VDD = 3  # V
    Capacity = 5  # Ah


    rx_current = 0.0142  # Amps   from the specification of RN2483
    node_max_period = 0
    noise_figure = 3  # db   For noise in the area of deployment of the node
    # MCU plus sensor current
    sensing_routine_consumption = 0.04651 # Joules
    sensing_routine_duration = 5.251 # seconds

    def __init__(self, id, x, y, n_single_rx_channels, sf_mask, network, role):
        self.x = x
        self.y = y
        self.id = id

        self.radio = radio.Lora_radio(env, x, y, 0, n_single_rx_channels, self.s, self, 125)
        self.battery = self.VDD * self.Capacity * 3600  # W x s= J
        self.battery_full = self.battery
        self.energy_from_rx = 0
        self.energy_from_tx = 0

        # Event counters
        self.start_sampling = env.event()
        self.sent = 0
        self.received = 0
        self.collisions = 0
        self.lost_changing_channel = 0

        self.mac = role(self, sf_mask, network)

        # linear regression from the values in RN2483 datasheet
        # Correlation Coefficient: r = 0.9992239468
        # Residual Sum of Squares: rss = 1.234618955
        # Coefficient of Determination: R2 = 0.9984484958

        self.tx_current = lambda pdbm: 1.173737973 * pdbm + 21.83836943 # milli Amperes

    def run(self):
        yield env.timeout(1)
        self.radio.get_neighbours()
        env.process(self.mac.run())
        if 'Node' in str(type(self.mac)):
            env.process(self.app())

    def app(self):
        if not self.start_sampling.processed: # when all nodes are awake
            yield self.start_sampling
        yield env.timeout(randrange(self.node_max_period))
        while True:
            if self.mac.connected:
                yield env.timeout(self.sensing_routine_duration)
                self.battery -= self.sensing_routine_consumption
                self.mac.data_pushed += 1
                self.mac.add_msg_to_send(self.mac.pack_msg('data', (self.id, 1, 2, 3, 4)))
                logger.error('[APP] %f  | %d pushed appdata, queue size %d' %
                             (env.now / 1000, self.id, len(self.mac.msgs_to_send)))
            #yield env.timeout(expovariate(1.0 / float(self.node_max_period)))
            yield env.timeout(self.node_max_period)

    def consume_energy_tx(self, power, airtime):
        energy = self.VDD * self.tx_current(power)/1000 * airtime   # volts x milliAmperes / 1000  x seconds = Joules
        self.battery = self.battery - energy
        self.energy_from_tx += energy

    def consume_energy_rx(self, duration):
        self.battery = self.battery - (self.VDD * self.rx_current * duration) * self.radio.max_single_demodulators
        self.energy_from_rx += self.VDD * self.rx_current * duration


    def __str__(self):
        return '    NODE ' + str(self.id)


def plot(size_x, size_y, nodes, bs):
    fig, ax = plt.subplots()

    ax.set_title("Spatial distribution of Nodes and Base Stations ")
    ax.set_xlim((0, size_x))
    ax.set_xlabel("position in meters")
    ax.set_ylim((0, size_y))
    ax.set_ylabel("position in meters")
    ax.set_xticks(range(0, size_x, ceil(size_x / 5)))
    ax.set_yticks(range(0, size_y, ceil(size_y / 5)))
    m_d = radio.max_distances(SX127x)
    for b in bs:
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[0],color='r',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[1],color='m',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[2],color='b',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[3],color='g',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[4],color='c',fill=False))
        ax.add_artist(plt.Circle((b.radio.x,b.radio.y),m_d[5],color='y',fill=False))
        ax.text(b.radio.x,b.radio.y,str(b.id),fontsize=5)
        print("BS: ", b.id, "N nodes: ", len(b.mac.nodes))
    for n in nodes:
        ax.text(n.radio.x, n.radio.y, str(n.id), fontsize=5)
    plt.grid()
    #plt.show()
    fig.savefig('plotcircles.png')


def reset_counters(nodes, bs):
    i = 0
    start_sampling = False
    print("setting up nodes, and connecting them with base stations...")
    while not start_sampling:
        connected = 0
        start_sampling = True
        for n in nodes:
            if not n.mac.connected:
                start_sampling = False
            else:
                connected += 1
        print(connected)
        yield env.timeout(Bs.min_epoch_duration)
        i += 1
    print("All nodes are connected. Starting sampling and resetting counters")

    for node in nodes:
        node.start_sampling.succeed()
    yield env.timeout(Bs.min_epoch_duration - (env.now - bs[0].mac.epoch_start))
    #print("started")
    for node in nodes:
        node.disconnects = 0
        node.sent = 0
        node.received = 0
        node.mac.msgs_to_send.clear()
        node.collisions = 0
        node.lost_changing_channel = 0
        node.mac.data_pushed = 0
        node.battery_full = SX127x.VDD * SX127x.Capacity * 3600
        node.battery = node.battery_full
        node.energy_from_rx = 0
        node.energy_from_tx = 0
        node.lbt_saves = 0

    for b in bs:
        b.disconnects = 0
        b.sent = 0
        b.received = 0
        b.mac.data_received = 0
        b.collisions = 0
        b.lost_changing_channel = 0
        b.battery_full = 0
        b.battery=0

    env.process(print_time(env.now))
    return env.now


def ms_to_human(milisecs):
    secs=milisecs/1000
    minutes=secs/60
    hours = minutes/60
    days = hours/24
    current_day_hours=hours%24
    current_hour_minutes=minutes%60
    return str(floor(days))+'d '+str(floor(current_day_hours))+'h '+str(floor(current_hour_minutes))+'m'


def print_time(init):
    while True:

        yield env.timeout(60 * 10 * 1000)
        print(ms_to_human(env.now - init))


if __name__ == "__main__":

    if len(sys.argv) < 7:
        print("usage: python sim.py station_positions.txt runtime ALOHA/ALOHA_SF_TSLOTS/ALOHA_SF_RESERVED_TSLOTS LogLevel NodePeriod EpochDuration")
        exit()

    if sys.argv[3] == 'ALOHA':
        from MAC.aloha import *
    elif sys.argv[3] == 'ALOHA_SF_TSLOTS':
        from MAC.slotted_aloha import *
    elif sys.argv[3] == 'RESERVED':
        from MAC.reserved_tslots import *
    elif sys.argv[3] == 'LORAWAN':
        from MAC.lorawan_B import *

    networks = []

    runtime = string_to_ms(sys.argv[2])
    SX127x.node_max_period = string_to_ms(sys.argv[5])
    Mac.min_epoch_duration = string_to_ms(sys.argv[6])#20 * 60 * 1000 # 20 minutes period for setting up the network
    f = open(sys.argv[1])
    content = f.readlines()
    i = 0
    for l_ in [line for line in content if '#' not in line]:
        l = l_.split()
        if 'size_x' in l:
            size_x = int(l[1])
        elif 'size_y' in l:
            size_y = int(l[1])
        elif 'network' in l:
            network = Network_server(int(l[1]), delay=string_to_ms(l[2]))
            networks.append(network)
        elif 'SX1272' in l:
            if l[3] == 'Node':
                n = SX127x(i, int(l[1]), int(l[2]), 1 , 0, network, Node)
                network.nodes.append(n)
            elif l[6] == 'Bs':
                n = SX127x(i, int(l[1]), int(l[2]), int(l[3]), int(l[5], 16), network, Bs)
                network.bs.append(n)
            env.process(n.run())
            i += 1
        elif 'SX1301' in l:
            b = Sx1301BS(i, int(l[1]), int(l[2]), int(l[3]), int(l[4]), int(l[5], 16), network, Bs)
            network.bs.append(b)
            env.process(b.run())
            i += 1
        elif 'gamma' in l:
            radio.gamma = float(l[1])
        elif 'd0' in l:
            radio.d0 = int(l[1])
        elif 'LPLD0' in l:
            radio.LPLd0 = float(l[1])

    logger = logging.getLogger('sim')
    logger.setLevel(logging.sys.argv[4])
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.sys.argv[4])
    logger.addHandler(ch)
    nodes = []
    bs = []
    for nw in networks:
        nodes.extend(nw.nodes)
        bs.extend(nw.bs)
    reset = env.process(reset_counters(nodes, bs))
    env.run(until=runtime)
    while True:
        try:
            env.run(until=runtime + reset.value)
            break
        except AttributeError:
            env.run(until=env.now + 20000)
    bat_full = nodes[0].battery_full
    min_battery = bat_full
    battery_accumulated = 0
    connected = 0
    for n in nodes:
        if n.mac.connected:
            connected += 1
            battery_accumulated += n.battery
            if n.battery < min_battery:
                min_battery = n.battery

    for nw in networks:
        nodes = nw.nodes
        bs = nw.bs
        results_nodes = [(n.mac.disconnects, n.sent, n.received,
                          n.collisions, n.mac.data_pushed, n.lost_changing_channel,
                          len(n.mac.msgs_to_send), n.energy_from_rx, n.energy_from_tx, n.mac.lbt_saves)
                         for n in nodes]
        results_bs = [(b.mac.disconnects, b.sent, b.received,
                       b.mac.data_received, b.collisions, b.lost_changing_channel, abs(b.battery)) for b in bs]

        total_results_nodes = tuple(map(sum, zip(*results_nodes)))
        total_energy = total_results_nodes[-2] + total_results_nodes[-3]
        ratio_rx_total = total_results_nodes[-3]/total_energy
        total_results_bs = tuple(map(sum, zip(*results_bs)))
        print("Report for network: ", nw.id)
        print("Nodes connected: ", connected, "average battery percentage: ", round(100*(battery_accumulated/len(nodes))/bat_full,3))
        print("\tDisconnects: %d\n\tSent: %d \n\tReceived: %d \n\tcollisions: %d \
              \n\tData Pushed: %d \n\tLost changing channel: %d\n\tData in queues: %d\n\t\
              Energy from receptions: %f\n\tEnergy from transmission: %f\n\tLBT saved: %d\n\tEnergy reception percentage: %f" %
              (*total_results_nodes, ratio_rx_total * 100))
        print("BS:")
        print("\tDisconnects: %d\n\tsent: %d \n\tReceived: %d \n\tData Received: %d \
                  \n\tCollisions: %d \n\tLost changing channel: %d\n\tEnergy Consumption: %f" %
              total_results_bs)

        print("\nTotal: \n\tLost uplinks: %d" %(total_results_nodes[1] - total_results_bs[2]))
        print("Data Extraction:", total_results_bs[3] * 100 / total_results_nodes[4])
        print("Minimum battery percentage: ", round(min_battery*100/bat_full,3))
        print("Expected Network LifeTime: ", ms_to_human(runtime * bat_full / (bat_full - min_battery)))

    plot(size_x, size_y, [n for n in nw.nodes for nw in networks], [bs for bs in nw.bs for nw in networks])





