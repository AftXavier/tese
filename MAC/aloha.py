from radio import uniformly_distributed_frequencies_with_margins, Packet
from settings import env
from .mac_base import *
from struct import pack, unpack
from random import randrange, shuffle


class Aloha(Mac):

    messages = {
        'beacon': (lambda dummy: '!BhiBh', 0,
                   'type origin cf n_channels total_bw'),
        'data': (lambda x: '!Bh' + 'BBBB', 3, 'type origin data')
    }

    def parse_packets(self, packets):
        return_dict = {}
        msg_types = self.messages.keys()
        for timestamp, pkt in packets:
            for msg_type in msg_types:
                aux = self.unpack_msg(pkt.payload, msg=msg_type)
                if aux is not None:
                    aux['timestamp'] = timestamp
                    aux['prx'] = pkt.prx
                    aux['type'] = msg_type
                    try:
                        tmp = return_dict[aux['origin']]
                        tmp.append(aux)
                        return_dict[aux['origin']] = tmp
                    except KeyError:
                        return_dict[aux['origin']] = [aux]
                    break
        return return_dict

    def pack_msg(self, msg, args):
        fmt = self.messages[msg][0]
        msg_type = self.messages[msg][1]
        if msg == 'ack':
            aux = [args[0]]
            for ID, prx in args[1:]:
                aux.append(ID)
                aux.append(prx)
            args = aux
        result = pack(fmt(int(len(args) / 2)), msg_type, *args)
        if msg != 'login' and msg != 'data':
            return self.encrypt(result)
        else:
            return result + self.digest_msg(result)

    def unpack_msg(self, payload, msg=None):
        if len(payload) > 37:
            payload = self.decrypt(payload)
        else:
            if not self.authenticate(payload):
                return None
            payload = payload[:-1]
        if msg is None:
            fmt = '!Bh' + 'B' * (len(payload) - 3)
            return unpack(fmt, payload)
        else:
            try:
                fmt, msg_type, fields = self.messages[msg]
                if msg == 'ack':
                    fmt_arg = int((len(payload) - 3) / 3)
                    fields = fields(fmt_arg * 3)
                else:
                    fmt_arg = 1
                a = unpack(fmt(fmt_arg), payload)
                if a[0] != msg_type:
                    return None
                else:
                    return {k: v for (k, v) in list(zip(fields.split(), a))}
            except Exception as e:
                return None

    def __init__(self, node, sf_mask, network):
        super(Aloha, self).__init__(node, sf_mask, network)


class Bs(Aloha):

    def __init__(self, station, sf_mask, network):
        super(Bs, self).__init__(station, sf_mask, network)
        self.msgs_to_send = deque()
        self.channel = []
        self.nodes = []
        self.sf_mask = sf_mask

        self.data_received = 0
        self.disconnects = 0

    def handle_recvs(self, pkt):
        msg = self.unpack_msg(pkt.payload)
        if msg[0] == self.messages['data'][1]:
            self.data_received += 1



    def run(self):
        bw = 125
        sf = max(self.get_sfs_from_sf_mask(self.sf_mask))
        yield env.timeout(self.start_delay)
        cf = yield env.process(self.find_clearest_frequency(bw))  # listens and chooses
        channel = (cf, sf, bw, 1)  # cr = 1
        self.radio.set_channel(channel)
        next_ul_duration = 60 * 1000
        while True:
            self.radio.set_channel(channel)
            yield env.process(self.uplink(next_ul_duration))
            next_ul_duration = yield env.process(self.downlink(channel))

    def uplink(self, ul_duration):
        self.epoch_start = env.now
        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d in uplink" % (env.now / 1000, self.id))
        yield env.process(self.radio.start_receiving(env))
        uplink_end = env.timeout(ul_duration)
        while True:
            pckt = env.process(self.radio.get_received_packet())
            unlock = yield uplink_end | pckt
            if uplink_end in unlock:
                break
            else:
                timestamp, recv = unlock[pckt]
                self.handle_recvs(recv)

    def downlink(self, channel):
        self.radio.stop_receiving(env)
        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d in downlink" % (env.now / 1000, self.id))

        beacon_msg = self.pack_msg('beacon', (self.id, channel[0], self.radio.n_channels,
                                              self.radio.total_bandwidth))
        beacon_airt = Packet(channel, payload=beacon_msg).airtime

        duty_cicle_implication = Mac.regulatory_parameters_from_frequency(channel[0])[0]

        next_ul_duration = max(beacon_airt / duty_cicle_implication, self.min_epoch_duration)
        beacon_msg = self.pack_msg('beacon', (self.id, channel[0], self.radio.n_channels,
                                              self.radio.total_bandwidth))
        yield env.process(self.send(beacon_msg, [(channel, 14)]))
        yield env.timeout(100)
        yield env.process(self.send(beacon_msg, [(self.DEFAULT_NW_LINK, 14)]))
        return next_ul_duration  # The uplink length sent to nodes


class Node(Aloha):

    def __init__(self, station, sf_mask, network):
        super(Node, self).__init__(station, sf_mask, network)

        self.connected = False
        self.to_send_flag = env.event()

        # adr variables
        self.msgs_to_send = deque()
        self.data_pushed = 0
        self.disconnects = 0

    def add_msg_to_send(self, msg, destination=None, left=False):
        if left:
            self.msgs_to_send.appendleft((msg, destination))
        else:
            self.msgs_to_send.append((msg, destination))
        self.to_send_flag.succeed()
        self.to_send_flag = env.event()

    def uplink(self, channels):
        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d in uplink" % (env.now / 1000, self.id))
        while True:
            try:
                msg = self.msgs_to_send.popleft()
            except IndexError:
                yield self.to_send_flag
                msg = self.msgs_to_send.popleft()

            logger.error("[MAC] %f | %d will send" % (env.now / 1000, self.id))
            shuffle(channels)
            yield env.process(self.send(msg[0], channels))

    def run(self):
        logger = logging.getLogger('sim')
        logger.error('[APP] %f || NODE %d came to life.' % (env.now / 1000, self.id))

        while True:
            if not self.connected:
                recv_pkts_dict = yield env.process(self.discovery_epoch(duration_seconds=randrange(10*60),
                                                                        packet_parser=self.parse_packets))
                beacons = self.get_beacons(recv_pkts_dict)
                if len(beacons) > 0:
                    beacons.sort(key=lambda x: -x['prx'])
                    best = beacons[0]['origin']
                    best_beacons = [b for b in beacons if b['origin'] == best]
                    best_beacons.sort(key=lambda x: -x['timestamp'])
                    pkt = best_beacons[0]
                    self.connected = True
                    cfs = uniformly_distributed_frequencies_with_margins(pkt['cf'], pkt['total_bw'], pkt['n_channels'])
                    channels = []
                    for cf in cfs:
                        channels.append(((cf, 12, 125, 1), 14))
            else:
                yield env.process(self.uplink(channels))


