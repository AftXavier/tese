from settings import env
from .mac_base import *
import numpy as np
from struct import pack, unpack
from math import ceil, floor
from random import randrange, choice, shuffle
from radio import uniformly_distributed_frequencies_with_margins, Packet
import simpy

SLOT_MARGIN = 250


class Frame:

    def __init__(self, start, channels, finish, origin):
        self.start_time = start
        self.origin = origin
        self.finish_time = finish
        self.channels = channels

    def __str__(self):
        return "" + str([c for c in self.channels])


class Epoch:

    max_payload_size_uplink = 'B' * 8 # the data message is 8 bytes long, considering the message authentication code

    @classmethod
    def slot_len(cls):
        max_pkt_airt = Packet(payload=cls.max_payload_size_uplink.encode(), param=(1, 12, 125, 1)).airtime
        return max_pkt_airt + SLOT_MARGIN

    def __init__(self, beacon, my_node_numbers):
        self.beacon = beacon
        self.origin = beacon['origin']
        try:
            self.node_number = my_node_numbers[self.origin]
        except KeyError:
            self.node_number = -1
        self.duration = beacon['duration'] * 1000
        self.time_to_ul_start = beacon['secs_to_ul'] * 1000
        self.start_time = beacon['timestamp'] + beacon['secs_to_ul'] * 1000
        self.finish_time = self.start_time + self.duration
        self.number_of_nodes = beacon['number_of_nodes'] + 1
        self.n_single_channels = beacon['n_single_channels']
        sf_mask = beacon['sf_mask']
        self.sf_mask = sf_mask
        self.slot_length = self.slot_len()
        self.sfs = Mac.get_sfs_from_sf_mask(beacon['sf_mask'])
        cfs = uniformly_distributed_frequencies_with_margins(beacon['cf'], beacon['total_bw'],
                                                             beacon['n_multi_channels'])
        self.channels = [(cf, sf, 125, 1) for cf, sf in np.array(np.meshgrid(cfs, self.sfs)).T.reshape(-1, 2)]
        self.downlink_channel = (beacon['cf'], 12, 125, 1)

    def get_next_frames(self, now, constraint_sf):
        cfs = [ceil(self.beacon['cf'] - i * int(125*1.5)) for i in range(self.n_single_channels)]
        frames_to_start = ceil(self.duration / self.slot_length)
        cf = self.beacon['cf']
        now = now if now > self.start_time else self.start_time
        if self.beacon['n_multi_channels'] == 0:
            for i in range(self.n_single_channels):
                elapsed = now - self.start_time
                elapsed_frames = floor(elapsed / self.slot_length)
                elapsed_this_frame = elapsed % self.slot_length
                current_nodes_frame = (elapsed_frames % self.number_of_nodes) - i * floor(self.number_of_nodes/self.n_single_channels)
                if current_nodes_frame < 0 :
                    current_nodes_frame = current_nodes_frame + self.number_of_nodes
                my_frame = self.node_number
                if my_frame == -1:
                    my_frame = self.number_of_nodes -1
                frames_to_my_frame = my_frame - current_nodes_frame
                if frames_to_my_frame < 0:
                    frames_to_my_frame += self.number_of_nodes
                potential_start = now + self.slot_length * frames_to_my_frame + self.slot_length - elapsed_this_frame
                if frames_to_my_frame < frames_to_start:
                    time_to_frame = potential_start
                    cf = cfs[i]
                    frames_to_start = frames_to_my_frame
            now = time_to_frame
            slots = []
            slot_init = now
            for sf in self.sfs:
                channels = [(cf, channel[1], channel[2], channel[3]) for channel in self.channels
                            if channel[1] == sf and constraint_sf(channel, self)]
                if len(channels) > 0:
                    slots.append(Frame(slot_init, channels, slot_init + SLOT_MARGIN, self.origin))
            if len(slots) > 0 :
                return slots
            return None
        else:
            slots = []
            for i, sf in enumerate(self.sfs):
                slot_init = now + self.slot_length
                channels = [channel for channel in self.channels if
                            channel[1] == sf and constraint_sf(channel, self)]
                if len(channels) > 0:
                    slots.append(Frame(slot_init, channels, slot_init + SLOT_MARGIN, self.origin))
            if len(slots) != 0:
                return slots
            return None

class ReservedTSlots(Mac):

    #   {
    #       message name :  ( format , msg type, elements)
    #   }
    messages = {'beacon': (lambda dummy: '!BHiBBHBHBH', 0,
                           'type  origin cf n_single_channels n_multi_channels total_bw secs_to_ul duration sf_mask number_of_nodes'),
                'login': (lambda dummy: '!BHH', 1, 'type  origin sink'),
                'ack': (lambda total_length: '!BH' + 'HBH' * total_length, 2,
                        lambda total_length: 'type  origin ' +
                                             ''.join([' {}'.format(i) for i in range(int(total_length))])),
                'sf_disclose': (lambda dummy: '!BHB', 5, 'type origin sf'),
                'data': (lambda x: '!BH' + 'BBBB', 3, 'type  origin data'),
                'adr': (lambda dummy: '!BHBBBBBBB', 4, 'type  origin s7 s8 s9 s10 s11 s12 losses')
                }

    def parse_packets(self, packets):
        return_dict = {}
        msg_types = self.messages.keys()
        for timestamp, pkt in packets:
            for msg_type in msg_types:
                aux = self.unpack_msg(pkt.payload, msg=msg_type)
                if aux is not None:
                    aux['timestamp'] = timestamp
                    aux['prx'] = pkt.prx
                    aux['type'] = msg_type
                    try:
                        tmp = return_dict[aux['origin']]
                        tmp.append(aux)
                        return_dict[aux['origin']] = tmp
                    except KeyError:
                        return_dict[aux['origin']] = [aux]
                    break
        return return_dict

    def pack_msg(self, msg, args):

        fmt = self.messages[msg][0]
        msg_type = self.messages[msg][1]
        if msg == 'ack':
            aux = [args[0]]
            for ID, prx, node_number in args[1:]:
                aux.append(ID)
                aux.append(prx)
                aux.append(node_number)
            args = aux
        result = pack(fmt(int(len(args) / 3)), msg_type, *args)
        if msg != 'login' and msg != 'data' and msg != 'sf_disclose':
            return self.encrypt(result)    # AES GCM with authentication tag for ACKS, beacons and ADR.
        else:
            return result + self.digest_msg(result)   # blake2s for uplink messages by nodes

    def unpack_msg(self, payload, msg=None):
        if len(payload) > 37:     # this payload may be encrypted
            payload = self.decrypt(payload)
            if payload is None:
                return None
        else:                     # if not it must be authenticated with blake2s
            if not self.authenticate(payload):
                return None
            payload = payload[:-1]
        if msg is None:
            fmt = '!Bh' + 'B' * (len(payload) - 3)
            return unpack(fmt, payload)
        else:
            try:
                fmt, msg_type, fields = self.messages[msg]
                if msg == 'ack':
                    fmt_arg = int((len(payload) - 3) / 5)
                    fields = fields(fmt_arg * 3)
                else:
                    fmt_arg = 1
                unpacked = unpack(fmt(fmt_arg), payload)
                if unpacked[0] != msg_type:
                    return None
                else:
                    return {k: v for (k, v) in list(zip(fields.split(), unpacked))}
            except Exception as e:
                return None

    def __init__(self, node, sf_mask, network):
        super(ReservedTSlots, self).__init__(node, sf_mask, network)


class Bs(ReservedTSlots):

    def __init__(self, station, sf_mask, network):
        super(ReservedTSlots, self).__init__(station, sf_mask, network)
        self.msgs_to_send = deque()
        self.to_be_ackd = []
        self.channel = []
        self.nodes = []
        self.sf_mask = sf_mask

        self.data_received = 0
        self.disconnects = 0
        self.nodes_sf = {}

    def run(self):
        bw = 125

        sf = max(self.get_sfs_from_sf_mask(self.sf_mask))
        yield env.timeout(self.start_delay)
        yield env.timeout(self.id * 5000)
        cf = yield env.process(self.find_clearest_frequency(bw))  # listens and chooses

        channel = (cf, sf, bw, 1)  # cr = 1
        next_ul_duration = 60 * 1000
        while True:
            self.radio.set_channel(channel)
            yield env.process(self.bs_uplink(next_ul_duration, channel, self.sf_mask))
            next_ul_duration = yield env.process(self.bs_downlink(channel, self.sf_mask))

    def handle_unslotted_uplink(self,channel):
        try:
            self.radio.set_channel(channel)
            yield env.process(self.radio.start_receiving(env))
            while True:
                pckt = yield env.process(self.radio.get_received_packet())
                timestamp, recv = pckt
                self.handle_recv(recv)
        except simpy.Interrupt:
            pass

    def handle_sf_slots_uplink(self, sf_mask, channel):
        slot_length = Epoch.slot_len()
        frame_number = -1
        self.radio.set_channel(channel)
        offset = floor((len(self.nodes) +1 )/ self.radio.max_single_demodulators)
        try:
            while True:
                for i, ch in enumerate(self.radio.single_sf_rx_channels):
                    node_number = (frame_number % (len(self.nodes) + 1)) - i * offset
                    if node_number < 0:
                        node_number = node_number + (len(self.nodes) + 1)
                    if node_number == len(self.nodes) :
                        self.radio.single_sf_rx_channels[i] = (ch[0], 12, ch[2], ch[3])
                    else:
                        try:
                            node_id = self.nodes[node_number]
                        except IndexError:
                            self.radio.single_sf_rx_channels[i] = (ch[0], 12, ch[2], ch[3])
                        else:
                            self.radio.single_sf_rx_channels[i] = (ch[0], self.nodes_sf[node_id], ch[2], ch[3])

                slot_end = env.timeout(slot_length)
                yield env.process(self.radio.start_receiving(env))
                while True:
                    pckt = env.process(self.radio.get_received_packet())
                    unlock = yield slot_end | pckt
                    if slot_end in unlock:
                        pckt.interrupt()
                        break
                    else:
                        timestamp, recv = unlock[pckt]
                        self.handle_recv(recv)
                self.radio.stop_receiving(env)
                frame_number += 1

        except simpy.Interrupt:
            self.radio.stop_receiving(env)
            pass

    def handle_recv(self, pkt):
        msg = self.unpack_msg(pkt.payload)
        if msg is None:
            self.discarded_msgs += 1
        if msg[0] == self.messages['login'][1]:
            msg = self.unpack_msg(pkt.payload, msg='login')
            if msg['sink'] == self.id:
                if msg['origin'] in self.nodes:
                    node_number = self.nodes.index(msg['origin'])
                else:
                    self.nodes.append(msg['origin'])
                    self.nodes_sf[msg['origin']] = 12
                    node_number = self.nodes.index(msg['origin'])
                self.to_be_ackd.append((msg['origin'], int(abs(pkt.prx)), node_number))
        elif msg[0] == self.messages['data'][1]:
            self.data_received += 1
        elif msg[0] == self.messages['sf_disclose'][1]:
            msg = self.unpack_msg(pkt.payload, msg='sf_disclose')
            self.nodes_sf[msg['origin']] = msg['sf']

    def bs_uplink(self, ul_duration, channel, sf_mask):
        logger = logging.getLogger('sim')
        self.epoch_start = env.now
        logger.error("[MAC] %f | %d in uplink" % (env.now / 1000, self.id))
        timeout = env.timeout(ul_duration)
        if self.radio.max_multi_sf_demodulators == 0:  # epoch with sf hops
            ul = env.process(self.handle_sf_slots_uplink(sf_mask, channel))
        else:  # No sf hop, all sfs allowed concurrently
            ul = env.process(self.handle_unslotted_uplink(channel))
        yield timeout
        ul.interrupt()
        logger.error("[MAC] %f | %d finished uplink" % (env.now / 1000, self.id))

    def get_sensitivities_and_losses(self):
        sensitivities = [abs(s[0]) for s in self.station.s]
        sensitivities.append(self.station.noise_figure)  # 3 is a noise figure
        return sensitivities

    def bs_downlink(self, channel, sf_mask):
        self.radio.stop_receiving(env)
        yield env.timeout(100)
        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d in downlink" % (env.now / 1000, self.id))

        sensitivities = self.get_sensitivities_and_losses()
        adr_msg = self.pack_msg('adr', (self.id, *sensitivities))
        adr_airt = Packet(channel, payload=adr_msg).airtime
        yield env.process(self.send(adr_msg, [(channel, 14)]))

        yield env.timeout(100)

        acks_airt = 0
        if len(self.to_be_ackd) > 0:
            acks_msg = self.pack_msg('ack', (self.id, *self.to_be_ackd))
            acks_airt = Packet(channel, payload=acks_msg).airtime
            yield env.process(self.send(acks_msg, [(channel, 14)]))
            self.to_be_ackd.clear()

        yield env.timeout(100)   # Regulated

        regulated = Mac.regulatory_parameters_from_frequency(channel[0])
        beacon_msg = self.pack_msg('beacon', (self.id, channel[0], self.radio.max_single_demodulators, self.radio.max_multi_sf_demodulators,
                                              self.radio.total_bandwidth, 1, 1, 0, 0))
        beacon_airt = Packet(channel, payload=beacon_msg).airtime

        # Either the uplink lasts for user specified time or Duty Cycle Consequence
        next_ul_duration = max((beacon_airt + acks_airt + adr_airt) / regulated[0],
                               self.min_epoch_duration)
        dur = ceil(next_ul_duration / 1000)
        # UL will start in 8 seconds
        yield env.timeout(8000 - 4000 - beacon_airt)
        offset = 4  # ul will start in 4
        beacon_msg = self.pack_msg('beacon', (self.id, channel[0], self.radio.max_single_demodulators, self.radio.max_multi_sf_demodulators,
                                              self.radio.total_bandwidth, offset, dur, sf_mask, len(self.nodes)))
        yield env.process(self.send(beacon_msg, [(channel, 14)]))
        yield env.timeout(4000 - beacon_airt)
        offset = 0
        beacon_msg = self.pack_msg('beacon', (self.id, channel[0], self.radio.max_single_demodulators, self.radio.max_multi_sf_demodulators,
                                              self.radio.total_bandwidth, offset, dur, sf_mask, len(self.nodes)))
        yield env.process(Mac.send(self, beacon_msg, [(self.DEFAULT_NW_LINK, 14)]))
        logger.error("[MAC] %f | %d finished downlink, next_ul_duration = %f" % (env.now / 1000, self.id, dur * 1000))

        return dur * 1000   # The uplink and downlink lengths sent to nodes


class Node(ReservedTSlots):

    def __init__(self, station, sf_mask, network):
        super(ReservedTSlots, self).__init__(station, sf_mask, network)

        self.connected = False
        self.to_send_flag = env.event()

        self.sinks = []
        self.connecting = []
        self.disconnects = 0

        # adr variables
        self.adrs = {}
        self.sink_prxs = {}
        self.adr_margin = 1
        self.sinks_ptx_per_sf = {}
        self.sink_node_number = {}

        self.to_be_ackd = []
        self.msgs_to_send = deque()

        self.data_pushed = 0

    def check_connections(self, recv_packets):
        recv_beacons = self.get_beacons(recv_packets)
        disconnections = [b for b in self.sinks if b not in map(lambda a: a['origin'], recv_beacons)]
        potential_connects = [b['origin'] for b in recv_beacons if b not in self.sinks]
        return disconnections, potential_connects

    def get_acks(self, recv_packets):  # still buggy
        acks = []
        for k, pkts in recv_packets.items():
            for pkt in pkts:
                if pkt['type'] == 'ack':
                    aux = pkt.copy()
                    aux.pop('type')
                    aux.pop('origin')
                    aux.pop('timestamp')
                    aux.pop('prx')
                    for key, value in aux.items():
                        if int(key) % 3 == 0:
                            if self.id == aux[key]:
                                prx_index = str(int(key) + 1)
                                node_number_index = str(int(key) + 2)
                                acks.append((pkt['origin'], aux[prx_index], aux[node_number_index]))
        return acks

    def node_downlink(self, channel):
        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d in downlink" % (env.now / 1000, self.id))
        self.radio.received_packets.clear()
        timeout = env.timeout(60 * 1000)  # 1 minute
        rec_packets = []
        dl_channel = (channel[0], 12, 125, 1)
        self.radio.set_channel(dl_channel)
        yield env.process(self.radio.start_receiving(env))
        while True:
            rec = env.process(self.radio.get_received_packet())
            lock = yield timeout | rec
            if timeout in lock:
                break
            else:
                rec_packets.append(lock[rec])
                tstamp, pkt = lock[rec]
                if self.unpack_msg(pkt.payload, msg='beacon'):
                    break
        next_packets = self.parse_packets(rec_packets)
        self.station.radio.stop_receiving(env)
        logger.error("[MAC] %f | %d in received: %s" % (env.now / 1000, self.station.id, next_packets))
        return next_packets

    def adr(self):
        sinks_ptx_per_sf = {}
        for sink in self.sinks + self.connecting:
            if sink in self.adrs.keys() and sink in self.sink_prxs.keys():
                sink_srx = [0 - s for s in self.adrs[sink][:-1]]
                recv_power_at_bs = - self.sink_prxs[sink]
                margin = [recv_power_at_bs - srx - self.adr_margin for srx in sink_srx]
                ptx_per_sf = [14 - m for m in margin]
                for i, p in enumerate(ptx_per_sf):
                    if p < -4:
                        ptx_per_sf[i] = -4
                    elif p > 14:
                        ptx_per_sf[i] = -5
                sinks_ptx_per_sf[sink] = ptx_per_sf
            else:
                sinks_ptx_per_sf[sink] = [14] * 6
        return sinks_ptx_per_sf

    def add_msg_to_send(self, msg, destination=None, left=False):
        if left:
            self.msgs_to_send.appendleft((msg, destination))
        else:
            self.msgs_to_send.append((msg, destination))
        self.to_send_flag.succeed()
        self.to_send_flag = env.event()

    def node_uplink(self, recv_packets):
        epochs = [Epoch(b, self.sink_node_number) for b in self.get_beacons(recv_packets)
                  if b['origin'] in self.sinks + self.connecting]

        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d started uplink" % (env.now / 1000, self.id))
        finish = max(epochs, key=lambda e: -e.finish_time).finish_time
        timeout = env.timeout(finish - env.now)
        while True:
            if finish - env.now > epochs[0].slot_length:
                try:
                    msg = self.msgs_to_send.popleft()
                except IndexError:
                    block = yield self.to_send_flag | timeout   # node in stop mode here
                    if timeout in block:
                        logger.error("[MAC] %f | %d uplink finished" % (env.now / 1000, self.id))
                        return epochs[0].downlink_channel
                    msg = self.msgs_to_send.popleft()

                constraint_sf = lambda channel, epoch: True
                if msg[1] is not None:
                    constraint_sf = lambda channel, epoch: channel[1] == max(epoch.sfs)

                lists_of_frames = [e.get_next_frames(env.now, constraint_sf) for e in epochs]
                all_frames = []
                for l in lists_of_frames:
                    all_frames.extend(l)
                frames_and_ptx = [(f, self.sinks_ptx_per_sf[f.origin][int(f.channels[0][1]-7)]) for f in all_frames
                                  if self.sinks_ptx_per_sf[f.origin][int(f.channels[0][1]-7)] > -5]
                self.sort_frames_by_energy_heuristic(frames_and_ptx)
                frame, ptx = frames_and_ptx[0]  # sorted list, these are the most energy efficient
                channels = [(c, ptx) for c in frame.channels]
                shuffle(channels)

                if frame.start_time < finish - epochs[0].slot_length :

                    yield env.timeout(frame.start_time - env.now +1)
                    logger.error("[MAC] %f | %d will send" % (env.now / 1000, self.id))
                    yield env.process(self.send(msg[0], channels))
                else:
                    self.add_msg_to_send(msg[0], destination=msg[1], left=True)
                    yield env.timeout(finish - env.now)
                    logger.error("[MAC] %f | %d uplink finished" % (env.now / 1000, self.id))
                    return epochs[0].downlink_channel
            else:
                yield env.timeout(finish - env.now)
                logger.error("[MAC] %f | %d uplink finished" % (env.now / 1000, self.id))
                return epochs[0].downlink_channel

    @staticmethod
    def sort_frames_by_energy_heuristic(frames_and_ptx):
        frames_and_ptx.sort(key=lambda x: Packet(x[0].channels[0], payload='payload', ptx=x[1]).energy_for_tx_heuristic)

    @staticmethod
    def sort_channels_by_energy_heuristic(channels_and_ptx):
        channels_and_ptx.sort(key=lambda x: Packet(x[0], payload='payload', ptx=x[1]).energy_for_tx_heuristic)

    def run(self):
        logger = logging.getLogger('sim')
        next_packets = None
        yield env.timeout(self.start_delay)
        yield env.timeout(self.id * Bs.min_epoch_duration *2)
        while True:
            logger.error('[MAC] %f || NODE %d is connected: %s' % (env.now / 1000, self.id, self.connected))
            if not self.connected:
                recv_pkts_dict = yield env.process(self.discovery_epoch(duration_seconds= randrange(10*60)
                                                                        , packet_parser=self.parse_packets))
                beacons = self.get_beacons(recv_pkts_dict)
                if len(beacons) > 0:
                    beacons.sort(key=lambda x: -x['prx'])
                    best = beacons[0]['origin']
                    best_beacons = [b for b in beacons if b['origin'] == best]
                    best_beacons.sort(key=lambda x: -x['timestamp'])
                    pkt = best_beacons[0]
                    self.connecting.append(best)
                    self.add_msg_to_send(self.pack_msg('login', (self.id, best)), destination=best)
                    next_packets = {best: [pkt]}

            if self.connected or len(self.connecting) > 0:
                self.sinks_ptx_per_sf = self.adr()

                recv_packets = next_packets
                # uplink
                channel = yield env.process(self.node_uplink(recv_packets))
                # downlink
                next_packets = yield env.process(self.node_downlink(channel))
                if len(self.get_beacons(next_packets)) == 0:
                    self.connecting.clear()
                    self.sinks.clear()
                    self.connected = False
                else:
                    disconnections, potential_connections = self.check_connections(next_packets)
                    self.disconnects += len(disconnections)
                    adrs = self.get_adrs(next_packets)
                    for a in adrs:
                        self.adrs[a['origin']] = [a['s7'], a['s8'], a['s9'], a['s10'], a['s11'], a['s12'], a['losses']]
                    connections = self.get_acks(next_packets)
                    for ID, prx, node_number in connections:
                        if ID in self.connecting or self.sinks:
                            self.sink_prxs[ID] = prx
                            self.sink_node_number[ID] = node_number
                            self.sinks_ptx_per_sf = self.adr()

                            if ID in self.connecting:
                                self.connected = True
                                ptx_per_sf = self.sinks_ptx_per_sf[ID]
                                channels = [(1,sf, 125, 1) for sf in range(7,13)]
                                all_channels_ptx = list(zip(channels, ptx_per_sf))
                                channels_ptx = []
                                for ch, ptx in all_channels_ptx:
                                    if ptx > -5:
                                        channels_ptx.append((ch,ptx))
                                self.sort_channels_by_energy_heuristic(channels_ptx)
                                my_least_consuming_sf = channels_ptx[0][0][1]
                                self.add_msg_to_send(self.pack_msg('sf_disclose', (self.id, my_least_consuming_sf)),destination=ID, left=True)

                    self.connecting.clear()
                    connections_ids = [c[0] for c in connections]
                    self.sinks = [s for s in self.sinks + connections_ids if s not in disconnections]
                    if len(self.sinks) == 0:
                        self.connecting.clear()
                        self.sinks.clear()
                        self.connected = False
