from hashlib import blake2s
import logging
from random import choice
from Crypto.Cipher import AES
from settings import env
from collections import deque


class Mac:
    min_epoch_duration = 0
    @classmethod
    def regulatory_parameters_from_frequency(cls, frequency):
        result = 0, 0, 0  # duty_cycle , max_ptx, label
        # subband h1.3, note 7 and note 5 and note 1
        if 865000 <= frequency < 868000:
            result = 0.01, 14, 'h1.3'
        # subband h1.4
        if 868000 <= frequency < 868600:
            result = 0.01, 14, 'h1.4'
        # subband h1.5
        if 868700 <= frequency < 869200:
            result = 0.001, 14, 'h1.5'
        # subband h1.6
        if 869400 <= frequency < 869650:
            result = 0.1, 27, 'h1.6'
        # subband h1.7
        if 869700 <= frequency < 870000:
            result = (1, 7, 'h1.7')
        return result

    @staticmethod
    def get_sfs_from_sf_mask(sf_mask):
        sfs = []
        for i in range(6):
            if sf_mask & 0x1:
                sfs.append(i + 7)
            sf_mask = sf_mask >> 1
        return sfs

    @staticmethod
    def get_beacons(recv_packets):
        beacons = []
        try:
            for k, pkts in recv_packets.items():
                for pkt in pkts:
                    if pkt['type'] == 'beacon':
                        beacons.append(pkt)
        except AttributeError:
            pass
        return beacons

    @staticmethod
    def get_adrs(recv_packets):
        adrs = []
        for k, pkts in recv_packets.items():
            for pkt in pkts:
                if pkt['type'] == 'adr':
                    adrs.append(pkt)
        return adrs

    def encrypt(self, msg):
        key = self.NETWORK_KEY
        aad = b''##self.auth_code
        cipher = AES.new(key, AES.MODE_GCM)
        cipher.update(aad)
        ciphertext, auth_tag = cipher.encrypt_and_digest(msg)
        nonce = cipher.nonce
        return nonce + aad + auth_tag + ciphertext

    def decrypt(self, msg):
        key = self.NETWORK_KEY
        nonce = msg[0:16]
        aad = b''#msg[16:20]
        tag = msg[16:32]
        ciphertext = msg[32:]
        cipher = AES.new(key, AES.MODE_GCM, nonce)
        cipher.update(aad)
        try:
            decrypted_msg = cipher.decrypt_and_verify(ciphertext, tag)
            return decrypted_msg
        except ValueError:
            return None

    def digest_msg(self, msg):
        hasher = blake2s(digest_size=1, key=self.NETWORK_KEY[-4:])
        hasher.update(msg)
        return hasher.digest()

    def authenticate(self, msg):
        digest = self.digest_msg(msg[:-1])
        if ord(digest) == msg[-1]:
            return True
        return False

    def __init__(self, station, sf_mask, network_server):
        self.id = station.id
        self.station = station
        self.radio = station.radio
        self.connected = False
        self.msgs_to_send = deque()
        self.to_send_flag = env.event()
        self.lbt_saves = 0
        self.channel = []
        self.epoch_start = 0
        self.NETWORK_KEY = network_server.NETWORK_KEY
        self.auth_code = network_server.auth_code
        self.DEFAULT_NW_LINK = network_server.MAC_DEFAULT_NW_LINK
        self.start_delay = network_server.delay
        self.discarded_msgs = 0

    def send(self, msg, transmission_params):

        #  Polite Spectrum Access, with LBT and AFA, if no channel is valid,
        if len(transmission_params) > 1:
            channel = transmission_params[0]
            clear = yield env.process(self.radio.clear_channel_assessment(channel[0], 10))  # value > 0.160ms regulated
            if clear:
                return (yield env.process(self.radio.send(msg, params=channel[0], ptx=channel[1])))
            else:
                self.lbt_saves +=1
                transmission_params.remove(channel)
                if len(transmission_params) > 0:
                    return (yield env.process(self.send(msg, transmission_params)))
                else:
                    return None
        # Duty Cycled send, not Polite, no channel assessment
        else:
            return (yield env.process(
                self.radio.send(msg, params=transmission_params[0][0], ptx=transmission_params[0][1])))

    def discovery_epoch(self, duration_seconds=None, packet_parser=None):
        logger = logging.getLogger('sim')
        logger.error("[MAC] %f | %d looking for beacons" % (env.now / 1000, self.id))
        self.radio.set_channel(self.DEFAULT_NW_LINK)
        yield env.process(self.radio.receive_during_seconds(duration_seconds))
        logger.error("[MAC] %f | %d finished looking for beacons" % (env.now / 1000, self.id))
        recv_packets = packet_parser(self.radio.received_packets)
        self.radio.received_packets.clear()
        return recv_packets

    def find_clearest_frequency(self, bw):
        bandwidthSlots = list(
            range(865000, 868000, bw + 25))  # possible frequencies, with 25 khz margin between channels
        for cf in bandwidthSlots:
            for sf in range(7,13):
                self.radio.set_channel((int(cf + bw/2), sf, 125, 1))
                yield env.process(self.radio.receive_during_seconds(20))  # 20 s * 6 sfs * 20 cfs if bw = 125

        bandwidthSlots = list(range(865000 + self.radio.max_single_demodulators * 150, 868000, bw + 25))    # possible frequencies, with 25 khz margin between channels
        counts = [0] * len(bandwidthSlots)
        for timestamp, packet in self.radio.received_packets:
            if 865000 <= packet.param[0] <= 868000:
                for i, slot in enumerate(bandwidthSlots):
                    if slot <= packet.param[0] <= bandwidthSlots[i+1]:
                        counts[i] += 1
        result = list(zip(bandwidthSlots, counts))
        min_count = min(counts)
        trimmed_result = [r for r in result if r[1] == min_count]
        clearest_slot, count = choice(trimmed_result)
        return clearest_slot + int(bw/2)
