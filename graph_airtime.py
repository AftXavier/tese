from radio import *

from matplotlib import pyplot as plt


fig,ax = plt.subplots(2)
fig.suptitle("Airtimes of Packets per payload with and without physical layer header",fontsize=15)
fig.subplots_adjust(hspace=.5)

param=(1,7,125,1)
packet = Packet(param,with_header=False)

results=[[],[],[],[],[],[]]
for c,sf in enumerate(range(7,13)):
    packet.param=(1,sf,125,1)
    i=0
    packet.payload=''
    while i < 50:
        packet.payload +='$'
        packet.payload.encode()
        print(len(packet.payload))
        results[c].append(packet.airtime)
        i+=1
    
    
 
ax[0].set_title('without Header')
ax[0].set_xlim((0,50))
ax[0].set_xticks(range(0,50,5))
ax[0].set_yticks(range(0,3000,200))
ax[0].set_xlabel("payload size in Bytes")
ax[0].set_ylabel('Airtime milisec')
ax[0].plot(range(0,50,1),results[0],label='SF7',color='y')
ax[0].plot(range(0,50,1),results[1],label='SF8',color='c')
ax[0].plot(range(0,50,1),results[2],label='SF9',color='g')
ax[0].plot(range(0,50,1),results[3],label='SF10',color='b')
ax[0].plot(range(0,50,1),results[4],label='SF11',color='m')
ax[0].plot(range(0,50,1),results[5],label='SF12',color='r')
ax[0].grid()
ax[0].legend()

param=(1,7,125,1)
packet = Packet(param,with_header=True)

results=[[],[],[],[],[],[]]
for c,sf in enumerate(range(7,13)):
    packet.param=(1,sf,125,1)
    i=0
    packet.payload=''
    while i < 50:
        packet.payload +='$'
        packet.payload.encode()
        print(len(packet.payload))
        results[c].append(packet.airtime)
        i+=1
ax[1].set_title(" with Header")
ax[1].set_xlim((0,50))
ax[1].set_xlabel("payload size in Bytes")
ax[1].set_ylabel('Airtime milisec')
ax[1].set_xticks(range(0,50,5))
ax[1].set_yticks(range(0,3000,200))
ax[1].plot(range(0,50,1),results[0],label='SF7',color='y')
ax[1].plot(range(0,50,1),results[1],label='SF8',color='c')
ax[1].plot(range(0,50,1),results[2],label='SF9',color='g')
ax[1].plot(range(0,50,1),results[3],label='SF10',color='b')
ax[1].plot(range(0,50,1),results[4],label='SF11',color='m')
ax[1].plot(range(0,50,1),results[5],label='SF12',color='r')
ax[1].legend()
ax[1].grid()

plt.show()
