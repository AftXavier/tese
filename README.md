# README #
0. First clone this repository:
git clone https://AftXavier@bitbucket.org/AftXavier/tese.git
1. Create a python3 virtual environment:
 MAC OSx machines require a framework install of python for matplotlib to work properly:
 This is built with python3 -m venv my\_virtualenv\_name

2. Activate the environment:
source my\_virtualenv\_name/bin/activate
Install the requirements:
	- pip install -r requirements.txt
	
3. To run a simulation:
python sim.py <topology_file.txt> duration <MAC> CRITICAL/INFO node_period epoch_duration
